package it.xpug.birthday_greetings;

public class SmtpMailService implements IMessageService {
	
	private String smtpHost;
	private int smtpPort;

	public SmtpMailService(String smtpHost, int smtpPort) {
		this.setSmtpHost(smtpHost);
		this.setSmtpPort(smtpPort);
	}

	public String getSmtpHost() {
		return smtpHost;
	}

	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}

	public int getSmtpPort() {
		return smtpPort;
	}

	public void setSmtpPort(int smtpPort) {
		this.smtpPort = smtpPort;
	}
}
