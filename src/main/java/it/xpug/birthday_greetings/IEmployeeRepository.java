package it.xpug.birthday_greetings;

import java.io.IOException;
import java.text.ParseException;

public interface IEmployeeRepository {

	public Employee findBirthdayEmployess(String employeesFile) throws IOException, ParseException; 
}
